---
date: "2016-05-05T21:48:51-07:00"
title: Liangmian
---

### Inhalt
- Weizennudeln 
- Gurke
- Seitan
- Erdnüsse
- Sojasauce, Chiliöl, Sesampaste, Reisessig, Knoblauch, Salz, Zucker

### Kontakt
Fabio Molo, [liangmian@protonmail.ch](mailto:liangmian@protonmail.ch)

V2020-09-20


