<!DOCTYPE html>
<html class="nojs" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes">
<title>A Plain Markdown Post – A Hugo website</title>
<meta name="created" content="2016-12-30T21:49:57-0700">
<meta name="modified" content="2016-12-30T21:49:57-0700">

<meta name="description" content="This is a post written in plain Markdown (*.md) instead of R Markdown (*.Rmd). The major differences are:  You cannot run any R code in a plain Markdown document, whereas in an R Markdown document, you can embed R code chunks (```{r}); A plain Markdown post is rendered through Blackfriday, and an R Markdown document is compiled by rmarkdown and Pandoc.  There are many differences in syntax between Blackfriday&amp;rsquo;s Markdown and Pandoc&amp;rsquo;s Markdown.">
<meta property="og:site_name" content="A Hugo website">
<meta property="og:title" content="A Plain Markdown Post">
<meta property="og:url" content="/2016/12/30/a-plain-markdown-post/">

<meta name="generator" content="Hugo 0.54.0" />


<link rel="canonical" href="/2016/12/30/a-plain-markdown-post/">
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="stylesheet" href="/css/styles.f4063a9fddb7050b7e0396a80151fb18f23a100970c4d935388d31783c41d706.css">
<link rel="stylesheet" href="/css/print.27fc184f8670f41a2690985390779e7b20335a8fadff8fa015cf9417ffe50c36.css" media="print">
<script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "BlogPosting",
    "headline": "A Plain Markdown Post",
    "datePublished": "2016-12-30T21:49:57-07:00",
    "dateModified": "2016-12-30T21:49:57-07:00",
    "url" : "/2016/12/30/a-plain-markdown-post/",
    "description": "This is a post written in plain Markdown (*.md) instead of R Markdown (*.Rmd). The major differences are:  You cannot run any R code in a plain Markdown document, whereas in an R Markdown document, you can embed R code chunks (```{r}); A plain Markdown post is rendered through Blackfriday, and an R Markdown document is compiled by rmarkdown and Pandoc.  There are many differences in syntax between Blackfriday\u0026rsquo;s Markdown and Pandoc\u0026rsquo;s Markdown.",
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": "/"
    },
    "publisher": {
      "@type": "Organization",
      "name": "A Hugo website",
      "url": "/"
    }
  }
</script>

<script defer src="/js/umbrella.min.b426107371d121c9a56a27aac9e9058ff592a021e2f4c3c94827fe70920d3cbb.js"></script>
<script defer src="/js/script.min.8e4d91ebe0d9226621e408b7205bd7b009e4935608e4b29c4275116490cc836d.js"></script>
</head>

<body>
<div class="page layout__page layout__sidebar-second">
<header class="header layout__header">
<a href="/" title="Home" rel="home" class="header__logo"><img src="/images/logo.png" alt="Home" class="header__logo-image"></a>
<h1 class="header__site-name">
<a href="/" title="Home" class="header__site-link" rel="home"><span>A Hugo website</span></a>
</h1>
<div class="region header__region">


</div>
</header>




<nav class="main-menu layout__navigation">
<h2 class="visually-hidden">Main menu</h2>
<ul class="navbar">
<li><a class="" href="/">Home</a></li>
<li><a class="" href="/about/">About</a></li>
<li><a class="active" href="/post/">Posts</a></li>
</ul>
</nav>


<main class="main layout__main">
<article class="section-post single-view">
<header>
<h1 class="title ">A Plain Markdown Post</h1>
</header>
<div class="content">
<p>This is a post written in plain Markdown (<code>*.md</code>) instead of R Markdown (<code>*.Rmd</code>). The major differences are:</p>

<ol>
<li>You cannot run any R code in a plain Markdown document, whereas in an R Markdown document, you can embed R code chunks (<code>```{r}</code>);</li>
<li>A plain Markdown post is rendered through <a href="https://gohugo.io/overview/configuration/">Blackfriday</a>, and an R Markdown document is compiled by <a href="http://rmarkdown.rstudio.com"><strong>rmarkdown</strong></a> and <a href="http://pandoc.org">Pandoc</a>.</li>
</ol>

<p>There are many differences in syntax between Blackfriday&rsquo;s Markdown and Pandoc&rsquo;s Markdown. For example, you can write a task list with Blackfriday but not with Pandoc:</p>

<ul class="task-list">
<li><label><input type="checkbox" checked disabled class="task-list-item"> Write an R package.</label></li>
<li><label><input type="checkbox" disabled class="task-list-item"> Write a book.</label></li>
<li><label><input type="checkbox" disabled class="task-list-item"> &hellip;</label></li>
<li><label><input type="checkbox" disabled class="task-list-item"> Profit!</label></li>
</ul>

<p>Similarly, Blackfriday does not support LaTeX math and Pandoc does. I have added the MathJax support to this theme (<a href="https://github.com/yihui/hugo-lithium">hugo-lithium</a>) but there is a caveat for plain Markdown posts: you have to include math expressions in a pair of backticks (inline: <code>`$ $`</code>; display style: <code>`$$ $$`</code>), e.g., <code>$S_n = \sum_{i=1}^n X_i$</code>.<sup class="footnote-ref" id="fnref:This-is-because"><a href="#fn:This-is-because">1</a></sup> For R Markdown posts, you do not need the backticks, because Pandoc can identify and process math expressions.</p>

<p>When creating a new post, you have to decide whether the post format is Markdown or R Markdown, and this can be done via the <code>ext</code> argument of the function <code>blogdown::new_post()</code>, e.g.</p>

<pre><code class="language-r">blogdown::new_post(&quot;Post Title&quot;, ext = '.Rmd')
</code></pre>
<div class="footnotes">

<hr />

<ol>
<li id="fn:This-is-because">This is because we have to protect the math expressions from being interpreted as Markdown. You may not need the backticks if your math expression does not contain any special Markdown syntax such as underscores or asterisks, but it is always a safer choice to use backticks. When you happen to have a pair of literal dollar signs inside the same element, you can escape one dollar sign, e.g., <code>\$50 and $100</code> renders &ldquo;\$50 and $100&rdquo;. <a class="footnote-return" href="#fnref:This-is-because"><sup>[return]</sup></a></li>
</ol>
</div>


</div>
</article>
</main>



<aside class="sidebar layout__second-sidebar">
<section>
<h4 class="menu"><a class="" href="/post/">Posts</a></h4>
<ul class="menu">
<li><a class="active" href="/2016/12/30/a-plain-markdown-post/">A Plain Markdown Post</a></li>
<li><a class="" href="/2015/07/23/hello-r-markdown/">Hello R Markdown</a></li>
<li><a class="" href="/2015/01/01/lorem-ipsum/">Lorem Ipsum</a></li>
</ul>
</section>
</aside>


<footer class="footer layout__footer">
<p><span>© A Hugo website</span></p>


</footer>

</div>
</body>
</html>
